import React from 'react';
import './App.css';
import { Route, Routes } from "react-router";
import { ActionIcon, AppShell, Group, Header, Kbd, Navbar, Text, Title, useMantineColorScheme } from '@mantine/core';
import { Home } from "./pages/Home";
import { About } from "./pages/About";
import { NotFound } from "./pages/NotFound";
import { OwnerRoomPage } from "./pages/OwnerRoomPage";
import { VinderApiClient } from "./backendConnection/vinderApiClient";
import { Links } from "./components/Links";
import { Rooms } from "./pages/Rooms";
import { Login } from "./pages/Login";
import { UserAuthProvider } from "./contexts/UserAuthContext";
import { UserControls } from "./components/UserControls";
import { BACKEND_BASE_URL } from "./BACKEND_CONFIG";
import { ParticipantRoomPage } from "./pages/ParticipantRoomPage";


function App() {
  const vinderApiClient = new VinderApiClient(BACKEND_BASE_URL, '/vinder');

  const { colorScheme, toggleColorScheme } = useMantineColorScheme();

  const links = [
    { name: 'Home', path: '/' },
    { name: 'About', path: '/about' },
    { name: 'Rooms', path: '/rooms' },
    { name: "Login", path: "/login" },
  ];

  return (
    <UserAuthProvider>
      <AppShell
        padding="md"
        navbar={
          <Navbar width={{ base: 300 }} height={500} padding="xs">
            <Navbar.Section grow mt="xs">
              <Links links={links}/>
            </Navbar.Section>
            <Navbar.Section>
              <UserControls/>
            </Navbar.Section>
          </Navbar>
        }
        header={
          <Header height={60}>
            <Group style={{ height: '100%', marginTop: 0, marginBottom: 0, paddingLeft: 20, paddingRight: 20, }}
                   position="apart">
              {/* TODO MJ: 2021-11-26  make it more lively and include the logo*/}
              {/*<img alt="Vinder logo" width={100}/>*/}
              <Title order={2} style={{ color: "#f59f00" }}>Vinder</Title>
              <>

                </>
              <Group>
                <Text><Kbd>⌘</Kbd>/<Kbd>ctrl</Kbd> + <Kbd>J</Kbd> to toggle dark mode</Text>
                <ActionIcon variant="default"
                            onClick={() => toggleColorScheme()}
                            color={colorScheme === 'dark' ? 'red' : 'blue'}
                            size={30}>
                </ActionIcon>
              </Group>
            </Group>
          </Header>
        }
        styles={(theme) => ({
          main: {
            backgroundColor:
              theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0],
          },
        })}
      >
        <Routes>
          <Route path={'/'} element={<Home/>}/>
          <Route path={'/about'} element={<About/>}/>
          <Route path={'/rooms'} element={<Rooms client={vinderApiClient}/>}/>
          <Route path={'/rooms/:id/owner'} element={<OwnerRoomPage client={vinderApiClient}/>}/>
          <Route path={'/rooms/:id/participant'} element={<ParticipantRoomPage client={vinderApiClient}/>}/>
          <Route path={'/login'} element={<Login/>}/>
          <Route path={'*'} element={<NotFound/>}/>
        </Routes>
      </AppShell>
    </UserAuthProvider>
  );
}

export default App;
