// TODO MJ: 2021-12-09 this structure resembles the session model returned by the API

// TODO MJ: 2021-12-08 refactor to resemble structure implemented in backend
import { Movie } from "./movie";

export class DbSessionModel {
  sessionId: string;
  participants: string[];
  votes: UserVotes[];
  toCheck: number[];

  constructor(sessionId: string, votes: UserVotes[], toCheck: number[]) {
    this.sessionId = sessionId;
    this.participants = votes.map(v => v.userId);
    this.votes = votes;
    this.toCheck = toCheck;
  }
}

export class UserVotes {
  userId: string;
  acceptedMovies: Movie[];
  declinedMovies: Movie[];

  constructor(userId: string, acceptedMovies: Movie[], declinedMovies: Movie[]) {
    this.userId = userId;
    this.acceptedMovies = acceptedMovies;
    this.declinedMovies = declinedMovies;
  }
}
