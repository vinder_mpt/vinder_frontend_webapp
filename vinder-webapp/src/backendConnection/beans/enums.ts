export const RoomModificationAction = {
  START: 'start',
  CLOSE: 'close',
};

export const MatchingProperty = {
  GET_PROPOSITIONS: 'get_propositions',
  GET_MATCHED: 'get_matched',
  GET_DECLINED: 'get_declined',
};