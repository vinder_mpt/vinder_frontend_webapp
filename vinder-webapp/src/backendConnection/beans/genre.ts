// TODO MJ: 2021-11-26 make it an enum?
// TODO MJ: 2021-11-26 add util methods here to map colors to genres
export interface Genre {
  genre_name: string;
}

export function getGenreColor(genre: Genre) {
  switch (genre.genre_name) {
    case 'Sci-Fi':
      return 'red';
    case 'Family':
      return 'yellow';
    case 'History':
      return 'dark';
    case 'Adventure':
      return 'orange';
    case 'Fantasy':
      return 'pink';
    case 'Film-Noir':
      return 'gray';
    case 'Sport':
      return 'indigo';
    case 'Musical':
      return 'red';
    case 'Crime':
      return 'pink';
    case 'Thriller':
      return 'pink';
    case 'Biography':
      return 'lime';
    case 'Romance':
      return 'red';
    case 'Animation':
      return 'teal';
    case 'Music':
      return 'red';
    case 'Mystery':
      return 'gray';
    case 'Comedy':
      return 'green';
    case 'Drama':
      return 'grape';
    case 'Western':
      return 'yellow';
    case 'Horror':
      return 'dark';
    case 'Action':
      return 'orange';
    case 'War':
      return 'gray';
  }
}

export function getGenreColorHex(genre: Genre) {
  switch (genre.genre_name) {
    case 'Sci-Fi':
      return '#495057';
    case 'Family':
      return '#40c057';
    case 'History':
      return '#f59f00';
    case 'Adventure':
      return '#d9480f';
    case 'Fantasy':
      return '#7950f2';
    case 'Film-Noir':
      return '#868e96';
    case 'Sport':
      return '#748ffc';
    case 'Musical':
      return '#fa5252';
    case 'Crime':
      return '#c92a2a';
    case 'Thriller':
      return '#364fc7';
    case 'Biography':
      return '#66d9e8';
    case 'Romance':
      return '#f783ac';
    case 'Animation':
      return '#ffe066';
    case 'Music':
      return '#fa5252';
    case 'Mystery':
      return '#e599f7';
    case 'Comedy':
      return '#69db7c';
    case 'Drama':
      return '#cc5de8';
    case 'Western':
      return '#e67700';
    case 'Horror':
      return '#c92a2a';
    case 'Action':
      return '#862e9c';
    case 'War':
      return '#2C2E33';
  }
}

const GENRE_NAMES = ['Music', 'Comedy', 'Horror', 'War', 'Mystery', 'Biography', 'History', 'Action', 'Sci-Fi', 'Western', 'Crime', 'Sport', 'Fantasy', 'Musical', 'Family', 'Romance', 'Thriller', 'Animation', 'Film-Noir', 'Adventure', 'Drama'];
export const GENRES = GENRE_NAMES.map(g => {return {'genre_name': g}});
