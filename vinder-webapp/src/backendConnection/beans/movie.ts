import { Genre } from "./genre";

export class Movie {
  id: number;
  genre: Genre[];
  poster_url: string;
  title: string;
  director: string;
  IMDb_rating: number;
  description: string;
  release_year: number;
  duration: number;
  cast: string;

  constructor(id: number,
              genre: Genre[],
              poster_url: string,
              title: string,
              director: string,
              IMDb_rating: number,
              description: string,
              release_year: number,
              duration: number,
              cast: string) {
    this.id = id;
    this.genre = genre;
    this.poster_url = poster_url;
    this.title = title;
    this.director = director;
    this.IMDb_rating = IMDb_rating;
    this.description = description;
    this.release_year = release_year;
    this.duration = duration;
    this.cast = cast;
  }
}

export const emptyMovie = {} as Movie;
