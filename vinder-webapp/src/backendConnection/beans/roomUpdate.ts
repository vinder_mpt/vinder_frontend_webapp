// TODO MJ: 2022-01-07 is it the correct model?
export class RoomUpdate {
  roomId: string;
  acceptedMoviesIds: number[];
  declinedMoviesIds: number[];
  userId: string;

  constructor(roomId: string, acceptedMoviesIds: number[], declinedMoviesIds: number[], userId: string) {
    this.roomId = roomId;
    this.acceptedMoviesIds = acceptedMoviesIds;
    this.declinedMoviesIds = declinedMoviesIds;
    this.userId = userId;
  }
}