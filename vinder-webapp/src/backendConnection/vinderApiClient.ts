import { Room } from "../types/room";
import { RoomUpdate } from "./beans/roomUpdate";
import axios from "axios";
import { Genre } from "./beans/genre";
import { MatchingProperty, RoomModificationAction } from "./beans/enums";

const staticSessions = require('../resources/sessions.json') as Room[];

export class VinderApiClient {
  hostname: string;
  rootApiPath: string;
  useMockData: boolean;

  constructor(hostname: string, rootApiPath: string, useMockData: boolean = false) {
    this.hostname = hostname;
    this.rootApiPath = rootApiPath;
    this.useMockData = useMockData;
  }

  async getPropositions(roomId: string) {
    return this.getRoomProperty(roomId, MatchingProperty.GET_PROPOSITIONS)
      .then(result => {
        if (result.data.success === 1) {
          return result.data.movies;
        } else {
          this.handleError(result.data)
        }
      })
      .catch(this.handleError);
  }

  async getPropositionsForParticipant(roomId: string) {
    return this.getRoomProperty(roomId, MatchingProperty.GET_PROPOSITIONS)
      .then(result => result.data)
      .catch(this.handleError);
  }

  async getMatched(roomId: string) {
    return this.getRoomProperty(roomId, MatchingProperty.GET_MATCHED);
  }

  async getDeclined(roomId: string) {
    return this.getRoomProperty(roomId, MatchingProperty.GET_DECLINED);
  }

  private async getRoomProperty(roomId: string, action: string) {
    return axios.get(`${this.hostname}${this.rootApiPath}/match/room/${roomId}/?action=${action}`,
      this.getAuthHeader());
  }

// TODO MJ: 2022-01-08 remove this and use modify room instead
  async updateRoom(roomUpdate: RoomUpdate) {
    return staticSessions.find(s => s.roomId === roomUpdate.roomId) || staticSessions[0];
  }

  async createRoom(genrePreferences: Genre[]): Promise<string> {
    return this.post(`/match/create_room/`,
      { genres: genrePreferences.map(g => g.genre_name) })
      .then(result => result.data.newRoomId)
      .catch(this.handleError);
  }

  async submitVote(roomId: string, movieId: string, accepted: boolean) {
    return this.post(`/match/room/${roomId}/`,
      { movieId: movieId, decision: accepted ? "yes" : "no" })
      .then(this.handleResult)
      .catch(this.handleError);
  }

  async joinRoom(roomId: string) {
    return this.post(`/match/join_room/`,
      { roomId: roomId })
      .then(this.handleResult)
      .catch(this.handleError)
  }

  async startRoom(roomId: string) {
    return this.modifyRoom(roomId, RoomModificationAction.START);
  }

  async closeRoom(roomId: string) {
    return await this.modifyRoom(roomId, RoomModificationAction.CLOSE);
  }

  private async modifyRoom(roomId: string, action: string) {
    return this.post(`/match/modify_room/`,
      { roomId: roomId, action: action })
      .then(this.handleResult)
      .catch(this.handleError);
  }

  private post = async (url: string, data: any) =>
    axios.post(`${this.hostname}${this.rootApiPath}${url}`, data, this.getAuthHeader());

// TODO MJ: 2022-01-08 add result interface
  private handleResult = (result: any) => {
    const data = result.data;
    if (data.success === 1) {
      console.log("SUCCESS!");
      console.log('got response', result);
      return 1;
    } else {
      this.handleError(data.msg);
      return 0;
    }
  };

  private handleError = (error: Error) => console.error('something went wrong', error);

  private getAuthHeader = () => ({ headers: { Authorization: `Bearer ${localStorage.getItem("vinder_access")}` } });
}
