import { Genre, GENRES, getGenreColor } from "../backendConnection/beans/genre";
import React, { useState } from "react";
import { Button } from "@mantine/core";
import _ from "lodash";

interface Props {
  onSubmit: (selectedGenres: Genre[]) => void;
}

export const GenreSelector = ({ onSubmit }: Props) => {
  const [selectedGenres, setSelectedGenres] = useState<Genre[]>([]);

  const isSelected = (genre: Genre) => !!_.find(selectedGenres, g => g === genre);
  const toggle = (genre: Genre) => setSelectedGenres(isSelected(genre) ?
    _.pull([...selectedGenres], genre) :
    [...selectedGenres, genre]);

  return (
    <>
      {GENRES.map((genre, index) =>
        <Button key={index}
                color={getGenreColor(genre)}
                size={'lg'}
                style={{ margin: 8 }}
                variant={isSelected(genre) ? "light" : "outline"}
                onClick={() => toggle(genre)}>
          {genre.genre_name}
        </Button>
      )}
      <Button size={'xl'} variant="light" color="yellow" style={{ marginTop: 20, width: 400 }}
              onClick={() => onSubmit(selectedGenres)}>
        DONE
      </Button>
    </>
  );
};
