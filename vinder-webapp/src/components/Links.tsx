import React from 'react';
import { createStyles } from '@mantine/styles';
import { Text, UnstyledButton } from "@mantine/core";
import { useNavigate } from "react-router-dom";

interface MainLinkProps {
  name: string;
  path: string;
  onClick?: () => void;
}

interface Props {
  links: MainLinkProps[]
}

const useStyles = createStyles((theme) => ({
  button: {
    display: 'block',
    width: '100%',
    padding: theme.spacing.xs,
    borderRadius: theme.radius.sm,
    color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,

    '&:hover': {
      backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
    },
  },
}));

const MainLink = ({ name, path, onClick }: MainLinkProps) => {
  const { classes } = useStyles();
  return (
    <UnstyledButton className={classes.button} onClick={onClick}>
      <Text size="lg">{name}</Text>
    </UnstyledButton>
  );
};

export const Links = ({ links }: Props) => {
  const navigate = useNavigate();
  return <div>{links.map((link) =>
    <MainLink name={link.name} path={link.path} onClick={() => navigate(link.path)} key={link.path}/>)}
  </div>;
};