import { Group, List, Spoiler, Title } from "@mantine/core";
import React from "react";
import { Movie } from "../backendConnection/beans/movie";

interface Props {
  roomId: string;
  matches: Movie[];
  acceptedMovies: Movie[];
  declinedMovies: Movie[];
}

interface VotingSpoilerProps {
  movies: Movie[];
  accepted: boolean;
}

const VotingSpoiler = ({ movies, accepted }: VotingSpoilerProps) => {
  const color = accepted ? "#2b8a3e" : "#c92a2a";
  const linkTitle = accepted ?
    'You gladly accepted the following movies.' :
    'You thoroughly dislike the following titles.';
  return (
    <Spoiler maxHeight={0} style={{ color: color }}
             showLabel={getMoviesSpoilerLabel(movies, accepted)}
             hideLabel={'Want them to disappear? Click here!'}>
      <Title order={4} style={{ color: color }}>{linkTitle}</Title>
      <List spacing='md' size='lg' center>
        {movies.map(movie => (
          <List.Item><span style={{ color: color, fontWeight: 700 }}>{movie.title}</span></List.Item>))}
      </List>
    </Spoiler>
  );
};

export const RoomDetails = ({ roomId, matches, acceptedMovies, declinedMovies }: Props) => {
  return (
    <Group direction={"column"}>
      <Title order={2} style={{ color: "#f59f00" }}>Details for room {roomId}</Title>

      {/*<Text>You're in a Vinder room with these people.</Text>*/}
      {/*<List spacing='sm' size='lg' center>*/}
      {/*  {room.participants.map((participant, index) => (<List.Item key={index}>{participant}</List.Item>))}*/}
      {/*</List>*/}
      {/*<Text size={'xs'} align={'right'}>*/}
      {/*  Don't know them? Please text them and see what happens, who knows...*/}
      {/*</Text>*/}

      <Title order={4} style={{ color: "#2b8a3e" }}>
        {matches.length >= 1 ?
          "You got the following matches!" :
          "Sadly there are no matches. Don't worry and try again!"}
      </Title>
      {matches.length > 0 &&
      <List spacing='md' size='lg' center>
        {matches.map(movie => (
          <List.Item><span style={{ color: "#2b8a3e", fontWeight: 700 }}>{movie.title}</span></List.Item>))}
      </List>}

      <VotingSpoiler movies={acceptedMovies} accepted={true}/>
      <VotingSpoiler movies={declinedMovies} accepted={false}/>

    </Group>
  );
};

function getMoviesSpoilerLabel(movies: Movie[], accepted: boolean): string {
  const length = movies.length;
  return `You ${accepted ? '' : 'dis'}liked ${length} ${length === 1 ? 'title' : 'titles'}. Want to see them? Sure!`;
}
