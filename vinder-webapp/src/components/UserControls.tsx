import React, { useContext } from "react";
import { UserAuthContext } from "../contexts/UserAuthContext";
import { Button, Group, PasswordInput, Text, TextInput } from "@mantine/core";
import { useInputState } from "@mantine/hooks";

export const UserControls = () => {
  const { login, anonymousLogin, logout, username, isAuthenticated } = useContext(UserAuthContext);
  const [loginValue, setLoginValue] = useInputState<string>('');
  const [passwordValue, setPasswordValue] = useInputState<string>('');
  const clearFields = () => {
    setLoginValue('');
    setPasswordValue('');
  };

  return (
    <Group direction={'column'} grow>
      {isAuthenticated && <>
          <Text size={'md'} style={{ margin: 0 }}>Hello {username}!</Text>
          <Button variant="light" color="red" radius="xl" size="sm" compact uppercase
                  onClick={() => logout && logout() && clearFields()}>
              logout
          </Button>
      </>}
      {!isAuthenticated && <>
          <TextInput value={loginValue} onChange={setLoginValue} placeholder="Name" size="md"/>
          <PasswordInput value={passwordValue} onChange={setPasswordValue} placeholder="Password" size="md"/>
          <Button variant="light" color="green" radius="xl" size="sm" compact uppercase
                  onClick={() => login && login(loginValue, passwordValue) && clearFields()}>
              login
          </Button>
          <Button variant="light" color="yellow" radius="xl" size="xs" compact uppercase
                  onClick={() => anonymousLogin && anonymousLogin()}>
              anonymous login
          </Button>
      </>}
    </Group>
  );
};
