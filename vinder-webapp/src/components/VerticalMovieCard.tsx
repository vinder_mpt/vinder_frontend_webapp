import { Badge, Button, Card, Group, Image, SimpleGrid, Spoiler, Text, Title, useMantineTheme } from '@mantine/core';
import React from 'react';
import { Genre, getGenreColor } from "../backendConnection/beans/genre";
import { Movie } from "../backendConnection/beans/movie";

interface Props {
  movie: Movie;
  onAccept: (movie: Movie) => void;
  onDecline: (movie: Movie) => void;
}

//@ts-ignore TODO: use VerticalMovieCard for mobile
export const VerticalMovieCard = ({ movie, onAccept, onDecline }: Props) => {
  const theme = useMantineTheme();
  const secondaryColor = theme.colorScheme === 'dark'
    ? theme.colors.dark[1]
    : theme.colors.gray[7];

  const getGenreBadge = (genre: Genre, index: number) =>
    <Badge key={index} color={getGenreColor(genre)} variant="light">{genre.genre_name}</Badge>;
  const getGenreBadges = (movie: Movie) =>
    <Group>{movie.genre.map((genreInfo, index) => getGenreBadge(genreInfo, index))}</Group>;

  return (
    <div style={{ width: 400, margin: 'auto', padding: 10 }}>
      <Card shadow="lg" padding="lg">
        <Card.Section>
          <Image src={movie.poster_url} alt="movie poster"/>
        </Card.Section>

        <Group position="apart" style={{ marginBottom: 5, marginTop: theme.spacing.sm }}>
          <Title order={3} style={{ color: "#f59f00" }}>{movie.title}</Title>
          {getGenreBadges(movie)}
          <Text size={'sm'} weight={700} style={{ color: secondaryColor, lineHeight: 1.5 }}>
            IMDb score: {movie.IMDb_rating}
          </Text>
        </Group>

        <Text size="sm" style={{ color: secondaryColor, lineHeight: 1.5 }}>
          {`${movie.duration} minutes, ${movie.release_year}`}
        </Text>
        <Text style={{ lineHeight: 1.5 }}>
          By {movie.director}
        </Text>
        <Text style={{ lineHeight: 1.5 }}>
          Starring: {movie.cast.split(',').join(', ')}
        </Text>

        <Spoiler maxHeight={36} initialState={false} showLabel="Read more" hideLabel="Hide description">
          <Text size={'md'}>{movie.description}</Text>
        </Spoiler>

        <SimpleGrid cols={2}>
          <Button variant="light" color="green" style={{ marginTop: 14 }} onClick={() => onAccept(movie)} fullWidth>
            👍
          </Button>
          <Button variant="light" color="red" style={{ marginTop: 14 }} onClick={() => onDecline(movie)} fullWidth>
            👎
          </Button>
        </SimpleGrid>
      </Card>
    </div>
  );
};