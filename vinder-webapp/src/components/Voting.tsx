import { Button, Drawer, Group, LoadingOverlay, Paper, Text, Title } from "@mantine/core";
import { HorizontalMovieCard } from "./HorizontalMovieCard";
import { RoomDetails } from "./RoomDetails";
import React, { useState } from "react";
import { Room } from "../types/room";
import { Movie } from "../backendConnection/beans/movie";
import { PromiseState } from "../hooks/usePromiseEffect";

interface Props {
  room: Room;
  movie: PromiseState<Movie>;
  onAccept: (movie: Movie) => void;
  onDecline: (movie: Movie) => void;
}

export const Voting = ({room, movie, onAccept, onDecline}: Props) => {
  const [drawerOpened, setDrawerOpened] = useState<boolean>(false);
  return (
    <Paper padding="lg" shadow="xl" radius="md" withBorder>
      <Title order={1} style={{ color: "#f59f00" }}>{`Room ${room.roomId}`}</Title>
      <Group direction={'column'} grow>
        <Text>
          Upvoted movies: {room.acceptedMovies.length}, downvoted
          movies: {room.declinedMovies.length}
        </Text>
        <Button variant="light" color="yellow" style={{ marginTop: 14, width: 300 }}
                onClick={() => setDrawerOpened(true)}>
          Voting summary
        </Button>
        {/*TODO MJ: 2021-11-28 handle potential errors*/}
        <LoadingOverlay visible={movie.status === 'idle' || movie.status === 'pending'}/>
        {movie.status === 'fulfilled' &&
        <HorizontalMovieCard movie={movie.value} onAccept={onAccept} onDecline={onDecline}/>}
      </Group>

      <Drawer size="40%"
              opened={drawerOpened}
              onClose={() => setDrawerOpened(false)}
              padding="xl"
              position="right">
        {/*<RoomDetails room={room}/>*/}
      </Drawer>

    </Paper>
  );
};