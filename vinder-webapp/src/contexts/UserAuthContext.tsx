import React, { FC, useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { BACKEND_BASE_URL } from "../BACKEND_CONFIG";

interface IUserAuthContext {
  userId: number;
  username: string;
  isAuthenticated: boolean;
  setNewUserId?: (newId: number) => void;
  setNewUsername?: (newUsername: string) => void;
  setNewIsAuthenticated?: (newIsAuthenticated: boolean) => void;
  login?: (username: string, password: string) => void;
  anonymousLogin?: () => void;
  refreshToken?: () => void;
  logout?: () => void;
}

const defaultUserAuthState = {
  userId: -1,
  username: "",
  isAuthenticated: false,
};

export const UserAuthContext = React.createContext<IUserAuthContext>(defaultUserAuthState);

export const UserAuthProvider: FC = ({ children }) => {
  // TODO: remove unnecessary console.logs
  const [isAuthenticated, setAuthenticated] = useState(!!localStorage.getItem("vinder_access"));
  const [username, setUsername] = React.useState("");
  const [userId, setUserId] = React.useState(-1);

  const setNewIsAuthenticated = (isAuth: boolean) => {
    setAuthenticated(isAuth);
  };

  const setNewUsername = (newUsername: string) => {
    setUsername(newUsername);
  };

  const setNewUserId = (newUserId: number) => {
    setUserId(newUserId);
  };

  const login = (username: string, password: string) => {
    axios
      .post(`${BACKEND_BASE_URL}/api/token/`, {
        username: username,
        password: password,
      })
      .then((response) => handleLoginResponse(response))
      .catch((error) => handleLoginError(error));
  };

  const anonymousLogin = () => {
    axios
      .get(`${BACKEND_BASE_URL}/vinder/anonymous_login/`)
      .then((response) => handleLoginResponse(response))
      .catch((error) => handleLoginError(error));
  };

  const handleLoginResponse = (response: AxiosResponse<any, any>) => {
    const { data: { access, refresh } } = response;
    localStorage.setItem("vinder_access", access);
    localStorage.setItem("vinder_refresh", refresh);
    setNewIsAuthenticated(true);
    console.log("LOGIN SUCCESSFUL!");
  };

  const handleLoginError = (error: { response: { status: number; }; }) => {
    console.error("Ooopsie");
    if (error.response) {
      // error came from the server
      if (error.response.status === 401) {
        console.error("Invalid credentials");
        // TODO MJ: 2021-12-14 throw an error to handle invalid credentials in the UI
      }
    }
  };

  const refreshToken = () => {
    axios
      .post(`${BACKEND_BASE_URL}/api/token/refresh/`, {
        refresh: localStorage.getItem("vinder_refresh"),
      })
      .then((response) => {
        const { data: { access, refresh } } = response;
        localStorage.setItem("vinder_access", access);
        localStorage.setItem("vinder_refresh", refresh);
        setNewIsAuthenticated(true);
        console.log("REFRESH SUCCESSFUL!");
      })
      .catch((error) => {
        if (error.response) {
          // error came from the server
          if (error.response.status === 401) {
            console.error("Invalid token or token expired");
            localStorage.removeItem("vinder_access");
            localStorage.removeItem("vinder_refresh");
          }
        }
      });
  };

  const logout = () => {
    localStorage.removeItem("vinder_access");
    localStorage.removeItem("vinder_refresh");
    setNewUsername("");
    setNewUserId(-1);
    setNewIsAuthenticated(false);
  };

  useEffect(() => {
    let source = axios.CancelToken.source();
    if (isAuthenticated && localStorage.getItem("vinder_access")) {
      // We just got a token, getting user info
      axios
        .get(`${BACKEND_BASE_URL}/vinder/get_user_data/`, {
          headers: { Authorization: `Bearer ${localStorage.getItem("vinder_access")}` },
          cancelToken: source.token,
        })
        .then((response) => {
          const { data: { id, username } } = response;
          setNewUserId(id);
          setNewUsername(username);
        })
        .catch((error) => {
          console.error("Oopsie, probably expired token");
          console.error(error);
        });
    }

    return () => {
      source.cancel("Cancelling in useEffect cleanup");
    }
  }, [isAuthenticated]);

  return (
    <UserAuthContext.Provider value={{
      userId,
      username,
      isAuthenticated,
      setNewUserId,
      setNewUsername,
      setNewIsAuthenticated,
      login,
      anonymousLogin,
      refreshToken,
      logout,
    }}
    >
      {children}
    </UserAuthContext.Provider>
  )
};
