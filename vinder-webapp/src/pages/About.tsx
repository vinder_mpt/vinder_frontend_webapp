import React from "react";
import { Paper, Text, Title } from "@mantine/core";

export const About = () =>
  (
    <Paper padding="lg" shadow="xl" radius="md" withBorder>
      <Title order={1}>About us</Title>
      <Text>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vitae ipsum finibus, interdum neque sed, aliquam
        elit. Proin a ipsum finibus, cursus velit id, faucibus magna. Sed scelerisque, lacus sit amet rhoncus mollis,
        enim lacus ultrices ligula, vel pharetra lorem magna ac tortor. Aliquam sed porttitor orci. Lorem ipsum dolor
        sit amet, consectetur adipiscing elit. Morbi elementum lectus a dui lacinia, quis mollis purus faucibus. Cras id
        feugiat nulla. Donec id mi eu libero lobortis tempus. Vestibulum quis ornare eros, eget interdum sem. Aliquam
        tempor lacus odio, quis dapibus mi euismod nec. Etiam sed metus nec nisi malesuada fermentum hendrerit at nibh.
        Morbi luctus ex sit amet tempor fringilla.
      </Text>
      <Text>
        Duis at nulla et ipsum vulputate venenatis imperdiet et dolor. Mauris massa ex, tempus blandit tellus sed,
        vehicula cursus felis. Proin placerat sem vitae bibendum tempus. Aliquam erat volutpat. Nulla vulputate, nisl eu
        vulputate convallis, lectus sapien mollis purus, non lacinia orci leo posuere enim. Nam pharetra dui sit amet
        pulvinar faucibus. Nulla facilisi. Vestibulum in dolor eget odio fermentum fermentum. Sed nec nibh id dolor
        scelerisque dapibus id nec quam. Nunc quis condimentum nisi, ac hendrerit arcu. Praesent diam nibh, dictum sit
        amet aliquet sed, congue eget ligula. Quisque eget tortor eget augue convallis feugiat nec quis magna. Nullam
        mollis, velit id ullamcorper aliquet, nisl urna accumsan quam, rhoncus luctus nisl dolor sed nibh. Vivamus risus
        metus, feugiat in mollis sit amet, lobortis sed orci. Nulla sed nibh vel sapien sollicitudin pulvinar.
        Suspendisse non turpis nec urna viverra accumsan a at ligula.
      </Text>
    </Paper>
  );