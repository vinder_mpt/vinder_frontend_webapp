import { Blockquote, Group, List, Paper, Text, Title } from "@mantine/core";
import React from "react";

export const Home = () =>
  (
    <Paper padding="lg" shadow="xl" radius="md" withBorder>
      <Title order={1}>Home</Title>
      <Group direction={'column'} grow>
        <Blockquote cite='– Vinder CEO' color='yellow'>
          <Text size={'xl'} weight={500}>
            Don't waste time before wasting more time!
          </Text>
        </Blockquote>
        <Group direction={'column'}>
          <Text size={'xl'}>
            Yes, you've heard that right. At Vinder we want you to enjoy the time you've set for relax.
            Here are some other things we don't want.
          </Text>
          <List spacing='md' size='xl' center>
            <List.Item>Your <span style={{ color: '#f59f00', fontWeight: 700}}>mood</span> ruined.</List.Item>
            <List.Item>Your <span style={{ color: '#f59f00', fontWeight: 700}}>iced</span> drink to get too hot.</List.Item>
            <List.Item>Your <span style={{ color: '#f59f00', fontWeight: 700}}>food</span> to get too cold.</List.Item>
            <List.Item>Your <span style={{ color: '#f59f00', fontWeight: 700}}>date</span> to lose interest in you.</List.Item>
          </List>
        </Group>
      </Group>
    </Paper>
  );