import React, { useContext, useState } from "react";
import { Button, Text } from "@mantine/core";
import { UserAuthContext } from "../contexts/UserAuthContext";
import axios from "axios";
import { BACKEND_BASE_URL } from "../BACKEND_CONFIG";

export const Login = () => {
  const { login, refreshToken, logout, userId, username, isAuthenticated } = useContext(UserAuthContext);

  const [roomId, setRoomId] = useState(-1);

  const createRoomRequest = () => {
    axios.post(`${BACKEND_BASE_URL}/vinder/match/create_room/`,
      { genres: ["Action", "Comedy"] },
      { headers: { Authorization: `Bearer ${localStorage.getItem("vinder_access")}` } })
      .then(function (result) {
        console.log(result.data);
        const newRoomId = result.data.newRoomId;
        setRoomId(newRoomId);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const joinRoomRequest = () => {
    axios.post(`${BACKEND_BASE_URL}/vinder/match/join_room/`,
      { roomId: roomId },
      { headers: { Authorization: `Bearer ${localStorage.getItem("vinder_access")}` } })
      .then((result) => {
        const data = result.data;
        if (data.success === 1) {
          console.log("SUCCESS!");
        } else {
          console.log(`ERROR: ${data.msg}`);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const startRoom = () => {
    axios.post(`${BACKEND_BASE_URL}/vinder/match/modify_room/`,
      { roomId: roomId, action: "start" },
      { headers: { Authorization: `Bearer ${localStorage.getItem("vinder_access")}` } })
      .then(result => {
        const data = result.data;
        if (data.success === 1) {
          console.log("SUCCESS!");
        } else {
          console.log(`ERROR: ${data.msg}`);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const closeRoom = () => {
    axios.post(`${BACKEND_BASE_URL}/vinder/match/modify_room/`,
      { roomId: roomId, action: "close" },
      { headers: { Authorization: `Bearer ${localStorage.getItem("vinder_access")}` } })
      .then(result => {
        const data = result.data;
        if (data.success === 1) {
          console.log("SUCCESS!");
        } else {
          console.log(`ERROR: ${data.msg}`);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const executeMovieAPICall = (roomID: number, action: string) => {
    axios.get(`${BACKEND_BASE_URL}/vinder/match/room/${roomID}/?action=${action}`,
      { headers: { Authorization: `Bearer ${localStorage.getItem("vinder_access")}` } })
      .then(result => {
        const data = result.data;
        console.log(data);
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <>
      <Button onClick={() => login && login("admin", "admin")}>
        LOGIN ADMIN
      </Button>
      <Button onClick={() => login && login("user1", "kopytko1")}>LOGIN TEST USER</Button>
      <Button onClick={() => refreshToken && refreshToken()}>
        TOKEN REFRESH SHOWCASE
      </Button>
      <Button onClick={() => logout && logout()}>
        LOGOUT SHOWCASE
      </Button>
      <Button onClick={() => createRoomRequest()}>CREATE ROOM</Button>
      <Button onClick={() => joinRoomRequest()}>JOIN ROOM</Button>
      <Button onClick={() => startRoom()}>START ROOM</Button>
      <Button onClick={() => closeRoom()}>CLOSE ROOM</Button>
      <Button onClick={() => roomId && executeMovieAPICall(roomId, "get_propositions")}>GET MOVIE PROPOSITIONS</Button>
      <Button onClick={() => roomId && executeMovieAPICall(roomId, "get_matched")}>GET MATCHED MOVIES</Button>
      <Button onClick={() => roomId && executeMovieAPICall(roomId, "get_declined")}>GET DECLINED MOVIES</Button>
      <Text>USER ID: {userId}</Text>
      <Text>USERNAME: {username}</Text>
      <Text>IS USER AUTHENTICATED: {isAuthenticated.toString()}</Text>
      <Text>ROOM ID: {roomId}</Text>
    </>
  );
};
