import React from "react";
import { Paper, Image, Title } from "@mantine/core";

export const NotFound = () =>
  (
    <Paper style={{ width: 720, margin: 'auto' }}>
      <Title order={1}>Nothing to see here!</Title>
      <Image
        radius="sm"
        src="https://http.cat/404.jpg"
        alt="status cat for http 404"
      />
    </Paper>
  );