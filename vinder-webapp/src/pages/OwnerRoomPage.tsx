import { Button, Drawer, SimpleGrid } from "@mantine/core";
import React, { useState } from "react";
import { VinderApiClient } from "../backendConnection/vinderApiClient";
import { Movie } from "../backendConnection/beans/movie";
import { usePromiseEffect } from "../hooks/usePromiseEffect";
import { useParams } from "react-router";
import { useListState } from "@mantine/hooks";
import { HorizontalMovieCard } from "../components/HorizontalMovieCard";
import { RoomDetails } from "../components/RoomDetails";

interface Props {
  client: VinderApiClient;
}

export const OwnerRoomPage = ({ client }: Props) => {
  const { id } = useParams();
  const roomId = validateRoomId(id);

  const [roomStarted, setRoomStarted] = useState<boolean>(false);
  const [roomClosed, setRoomClosed] = useState<boolean>(false);
  const [drawerOpened, setDrawerOpened] = useState<boolean>(false);
  const flipDrawerOpened = () => setDrawerOpened(curr => !curr);
  const [updatesCounter, setUpdatesCounter] = useState<number>(0);
  const [matchedFetchCounter, setMatchesFetchCounter] = useState<number>(0);

  const moviePropositions = usePromiseEffect<Movie[]>(async () => {
    return roomStarted ?
      await client.getPropositions(roomId) :
      [];
  }, [roomStarted, updatesCounter]);

  const matches = usePromiseEffect<Movie[]>(async () =>
      await client.getMatched(roomId).then(response => response.data.movies),
    [matchedFetchCounter]);

  const [acceptedMovies, acceptedHandlers] = useListState<Movie>([]);
  const [declinedMovies, declinedHandlers] = useListState<Movie>([]);

  const vote = (movie: Movie, accepted: boolean): void => {
    accepted ? acceptedHandlers.append(movie) : declinedHandlers.append(movie);
    // @ts-ignore
    moviePropositions.value = moviePropositions.value.slice(1);
    client.submitVote(roomId, movie.id.toString(), accepted)
      .then(ignore => setMatchesFetchCounter(curr => curr + 1));
    moviePropositions.value.length === 0 && setUpdatesCounter(current => current + 1);
  };

  const startRoom = () => {
    client.startRoom(roomId).then(result => {
      setRoomStarted(result === 1);
      setRoomClosed(false);
    });
  };
  const closeRoom = () => {
    client.closeRoom(roomId).then(result => {
      setRoomClosed(result === 1);
      setRoomStarted(false);
      flipDrawerOpened();
    });
  };

  return <>
    <SimpleGrid cols={3}>
      <Button variant='light' radius='xl' size='md' color='green' onClick={startRoom}>START THE ROOM</Button>
      <Button variant='light' radius='xl' size='md' color='red' onClick={closeRoom}>CLOSE THE ROOM</Button>
      <Button variant='light' radius='xl' size='md' color='yellow' onClick={flipDrawerOpened}>CHECK THE MATCHES</Button>
    </SimpleGrid>

    {roomStarted && moviePropositions.status === 'fulfilled' && moviePropositions.value.length > 0 &&
    <HorizontalMovieCard movie={moviePropositions.value[0]}
                         onAccept={movie => vote(movie, true)}
                         onDecline={movie => vote(movie, false)}/>
    }

    <Drawer size="40%"
            opened={drawerOpened}
            onClose={flipDrawerOpened}
            padding="xl"
            position="right">
      {matches.status === 'fulfilled' && <RoomDetails roomId={roomId}
                                                      matches={matches.value}
                                                      acceptedMovies={acceptedMovies}
                                                      declinedMovies={declinedMovies}/>}
    </Drawer>

  </>;
};

//not sophisticated at all
const validateRoomId = (id: any): string => id || 'invalid_room_id';