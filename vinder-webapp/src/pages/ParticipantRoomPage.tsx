import { Button, Drawer, Text } from "@mantine/core";
import React, { useEffect, useState } from "react";
import { VinderApiClient } from "../backendConnection/vinderApiClient";
import { Movie } from "../backendConnection/beans/movie";
import { usePromiseEffect } from "../hooks/usePromiseEffect";
import { useParams } from "react-router";
import { useInterval, useListState } from "@mantine/hooks";
import { HorizontalMovieCard } from "../components/HorizontalMovieCard";
import { RoomDetails } from "../components/RoomDetails";

interface Props {
  client: VinderApiClient;
}

export const ParticipantRoomPage = ({ client }: Props) => {
  const { id } = useParams();
  const roomId = validateRoomId(id);

  //periodically fetch movie propositions
  const refreshMillis = 3000;
  const [refreshCounter, setRefreshCounter] = useState<number>(0);
  const interval = useInterval(() => setRefreshCounter(current => current + 1), refreshMillis);
  useEffect(() => {
    client.joinRoom(roomId);
    interval.start();
    return interval.stop;
  }, []);

  const [roomStarted, setRoomStarted] = useState<boolean>(false);
  const [drawerOpened, setDrawerOpened] = useState<boolean>(false);
  const flipDrawerOpened = () => setDrawerOpened(curr => !curr);
  const [updatesCounter, setUpdatesCounter] = useState<number>(0);
  const [matchedFetchCounter, setMatchesFetchCounter] = useState<number>(0);

  const moviePropositions = usePromiseEffect<Movie[]>(async () => {
    return await client.getPropositionsForParticipant(roomId)
      .then(data => {
        if (data.success === 1) {
          interval.stop();
          setRoomStarted(true);
          return data.movies;
        } else if (data.success === 0) {
          return [];
        }
      });
  }, [updatesCounter, refreshCounter]);

  const matches = usePromiseEffect<Movie[]>(async () =>
      await client.getMatched(roomId).then(response => response.data.movies),
    [matchedFetchCounter]);

  const [acceptedMovies, acceptedHandlers] = useListState<Movie>([]);
  const [declinedMovies, declinedHandlers] = useListState<Movie>([]);

  const vote = (movie: Movie, accepted: boolean): void => {
    accepted ? acceptedHandlers.append(movie) : declinedHandlers.append(movie);
    // @ts-ignore
    moviePropositions.value = moviePropositions.value.slice(1);
    client.submitVote(roomId, movie.id.toString(), accepted)
      .then(submissionResult => {
        submissionResult === 1 && setMatchesFetchCounter(curr => curr + 1);
        if (submissionResult !== 1) {
          setRoomStarted(false);
          setDrawerOpened(true);
        }
      });
    moviePropositions.value.length === 0 && setUpdatesCounter(current => current + 1);
  };

  return <>
    <Button variant='light' radius='xl' size='md' color='yellow' onClick={flipDrawerOpened}>CHECK THE MATCHES</Button>

    {roomStarted && moviePropositions.status === 'fulfilled' && moviePropositions.value.length > 0 &&
    <HorizontalMovieCard movie={moviePropositions.value[0]}
                         onAccept={movie => vote(movie, true)}
                         onDecline={movie => vote(movie, false)}/>
    }

    <Drawer size="40%"
            opened={drawerOpened}
            onClose={() => setDrawerOpened(false)}
            padding="xl"
            position="right">
      {matches.status === 'fulfilled' && <RoomDetails roomId={roomId}
                                                      matches={matches.value}
                                                      acceptedMovies={acceptedMovies}
                                                      declinedMovies={declinedMovies}/>}
    </Drawer>

  </>;
};

//not sophisticated at all
const validateRoomId = (id: any): string => id || 'invalid_room_id';