import { VinderApiClient } from "../backendConnection/vinderApiClient";
import { Button, Group, LoadingOverlay, Modal, Paper, TextInput, Title } from "@mantine/core";
import React, { useState } from "react";
import { GenreSelector } from "../components/GenreSelector";
import { useNavigate } from "react-router-dom";

interface Props {
  //user id? -- get it from the context
  client: VinderApiClient;
}

export const Rooms = ({ client }: Props) => {
  const [modalOpened, setModalOpened] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [roomToJoin, setRoomToJoin] = useState<string>('');

  const navigate = useNavigate();
  const navigateToRoom = (roomId: string, owner: boolean) =>
    navigate(`/rooms/${roomId}/${owner ? 'owner' : 'participant'}`);

  return <Paper padding="lg" shadow="xl" radius="md" withBorder>
    <Title order={1} style={{ color: "#f59f00" }}>{Rooms}</Title>
    <Group grow>
      <Group direction={'column'}>
        <Title order={2} style={{ color: "#f59f00" }}>Join an existing session...</Title>
        {/*TODO: scan a QR code here?*/}
        <TextInput
          size={'xl'}
          placeholder="Enter room number here"
          style={{ marginTop: 20, width: 400 }}
          onChange={(event) => {
            // TODO MJ: 2021-12-10 validate the room id
            setRoomToJoin(event.currentTarget.value);
          }}
          rightSectionWidth={100}
          rightSection={
            <Button variant="light" color="yellow" radius="xl" size="xs" compact uppercase
                    onClick={() => navigateToRoom(roomToJoin, false)}>
              join
            </Button>}
        />
      </Group>

      <Group direction={'column'}>
        <Title order={2}>...or start your own!</Title>
        <Button size={'xl'} variant="light" color="yellow" style={{ marginTop: 20, width: 400 }}
                onClick={() => setModalOpened(true)}>
          Pick genres and get to it!
        </Button>
      </Group>
    </Group>

    <Modal size='lg'
           overflow={'inside'}
           opened={modalOpened}
           onClose={() => setModalOpened(false)}
           title={'Pick what you want to watch'}>
      <GenreSelector onSubmit={(genrePreferences) => {
        setLoading(true);
        setModalOpened(false);
        // TODO MJ: 2022-01-07 show QR code here
        client.createRoom(genrePreferences).then(roomId => navigateToRoom(roomId, true));
      }}/>
    </Modal>

    <LoadingOverlay visible={loading}/>
  </Paper>;
};
