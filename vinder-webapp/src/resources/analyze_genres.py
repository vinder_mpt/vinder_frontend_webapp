from csv import reader

lines = (line[5] for (i, line) in enumerate(reader(open('./imdb_top_1000.csv', 'r'))) if i > 0)
nested_lists = (word for word in (words.split(',') for words in lines))
genres = set(w.strip() for words in nested_lists for w in words)

for g in genres:
    print(f"case: '{g}'")
