import { Movie } from "../backendConnection/beans/movie";

// TODO MJ: 2021-12-08 refactor to resemble structure implemented in backend
// TODO MJ: 2022-01-04 rename to Room and match with class Room(models.Model):
export class Room {
  roomId: string;
  // TODO MJ: 2022-01-07 no participants in Room model?
  participants: string[];
  acceptedMovies: Movie[];
  declinedMovies: Movie[];
  toCheck: number[];

  constructor(roomId: string, participants: string[], toCheck: number[]) {
    this.roomId = roomId;
    this.participants = participants;
    this.acceptedMovies = [];
    this.declinedMovies = [];
    this.toCheck = toCheck;
  }
}
